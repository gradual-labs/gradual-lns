# holoLNS


## Location Naming Service

 *holo*LNS is a service that locates anything !


How does it works,
it takes a "descriptive address" and transformed it into a "canonical" namespace
that is globally registered for indexing and discovery.


Ex:

 - uri:address:firstname@zipcode
 - uri:inbox:user@org
 - uri:people:fullname+id7@city
 - uri:people:@publicname
 - uri:bot:email
 - uri:org:info@orgname+shard
 - uri:ipns:peerid
 - uri:ipfs:sha256
 - uri:mfs:/my/friends/peerids.yml
 - uri:mfs:/my/bots/peerids.yml
 - uri:ns@org:localpath
 - uri:qgit:org/proj/repository
 - uri:qwiki:smug
 - uri:dns:cname/path
 - uri:file:/home/user/file.txt

 - urn:nickname:...


 $Source: gitlab@kinLabs:holoLNS/README.md $
